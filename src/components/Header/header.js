import React from 'react';
import * as sectionTypes from './sectionTypes';
import * as sortTypes from './sortTypes';
import * as windowTypes from './windowTypes';
import { Link } from 'react-router-dom';

import  './header.style.scss';

const header = (props) => (

    <>
        <header className="header">
            <div className="header-wrapper flex-grid-thirds">
                <div className="header-logo col">
                    <Link to={'/gallery'}>
                         Gallery
                    </Link>
                </div>
                {props.nav === "true" ?
                    <div className="col">
                        <nav className="section">
                            <ul className="mainMenu">
                                <li
                                    className={(props.section === sectionTypes.SECTION_HOT) ? 'active' : ''}
                                    onClick={() => (props.sectionClicked(sectionTypes.SECTION_HOT))}>
                                    {sectionTypes.SECTION_HOT}</li>

                                <li className={(props.section === sectionTypes.SECTION_TOP) ? 'active' : ''}
                                    onClick={() => (props.sectionClicked(sectionTypes.SECTION_TOP))}>{sectionTypes.SECTION_TOP}</li>
                                <li className={(props.section === sectionTypes.SECTION_USER) ? 'active' : ''}
                                    onClick={() => (props.sectionClicked(sectionTypes.SECTION_USER))}>{sectionTypes.SECTION_USER}</li>
                            </ul>
                        </nav>
                    </div> : null
                }

                {props.section === sectionTypes.SECTION_USER ?
                    <div className="col">
                        <div className="subMenu">
                            <nav>
                                <ul>
                                    <li
                                        className={(props.sort === sortTypes.SORT_VIRAL) ? 'active' : ''}
                                        onClick={() => (props.sortClicked(sortTypes.SORT_VIRAL))}>
                                        {sortTypes.SORT_VIRAL}</li>

                                    <li className={(props.sort === sortTypes.SORT_TOP) ? 'active' : ''}
                                        onClick={() => (props.sortClicked(sortTypes.SORT_TOP))}>{sortTypes.SORT_TOP}</li>
                                    <li className={(props.sort === sortTypes.SORT_TIME) ? 'active' : ''}
                                        onClick={() => (props.sortClicked(sortTypes.SORT_TIME))}>{sortTypes.SORT_TIME}</li>
                                </ul>
                            </nav>
                        </div>
                    </div> : null
                }
                {props.section === sectionTypes.SECTION_TOP ?
                    <div className="col">
                        <div className="subMenu">
                            <nav className="windowSort">
                                <ul>
                                    <li
                                        className={(props.window === windowTypes.WINDOW_DAY) ? 'active' : ''}
                                        onClick={() => (props.windowClicked(windowTypes.WINDOW_DAY))}>
                                        {windowTypes.WINDOW_DAY}</li>
                                    <li
                                        className={(props.window === windowTypes.WINDOW_WEEK) ? 'active' : ''}
                                        onClick={() => (props.windowClicked(windowTypes.WINDOW_WEEK))}>
                                        {windowTypes.WINDOW_WEEK}</li>
                                    <li
                                        className={(props.window === windowTypes.WINDOW_MONTH) ? 'active' : ''}
                                        onClick={() => (props.windowClicked(windowTypes.WINDOW_MONTH))}>
                                        {windowTypes.WINDOW_MONTH}</li>
                                    <li
                                        className={(props.window === windowTypes.WINDOW_YEAR) ? 'active' : ''}
                                        onClick={() => (props.windowClicked(windowTypes.WINDOW_YEAR))}>
                                        {windowTypes.WINDOW_YEAR}</li>
                                    <li
                                        className={(props.window === windowTypes.WINDOW_ALL) ? 'active' : ''}
                                        onClick={() => (props.windowClicked(windowTypes.WINDOW_ALL))}>
                                        {windowTypes.WINDOW_ALL}</li>
                                </ul>
                            </nav>
                        </div>
                    </div> : null
                }

                {props.section === sectionTypes.SECTION_HOT ?
                    <div className="col">

                    </div> : null
                }

                {props.nav === 'true' ?
                    <div className="col">
                        <div className="colContainer">
                            <label className="switch">
                                <input type="checkbox"
                                    name="showViral"
                                    checked={props.showViral}
                                    onChange={() => props.viralChanged(props.showViral)} />
                                <span className="slider round"></span>
                            </label>
                            <span className="viralLabel">Show Viral Images {props.viral}</span>
                        </div>
                    </div> : null
                }
            </div>
        </header>
    </>
);

export default header;
