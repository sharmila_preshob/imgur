import React from 'react';
import './fourofour.style.scss'
import Header from '../Header/header';
import { Link } from 'react-router-dom';

const FourOFour = (props) => (

    <>
        <Header nav="false"></Header>
        <div className="heading">404</div>
        <div className="description">The requested Page doesn't exist</div>
        <div className="link">
            <Link className="link" to="/gallery">Go back to Gallery</Link>
        </div>
    </>
);


export default FourOFour;
